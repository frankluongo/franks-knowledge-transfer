# Fiscal / Econ Section of Website

I was approached by some folks on the Fiscal Team to figure out a design on the current website that would incorporate some new departments being added to the [Legislative Council Staff Page](http://leg.colorado.gov/agencies/legislative-council-staff)

I created some designs and sent them for review, but this never went anywhere
