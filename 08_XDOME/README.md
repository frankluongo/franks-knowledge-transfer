# XDOME

## Assets

This folder contains any reference imagery used to help design the application

## Design

### Application

All files are named `XD UI V1 IT#`, which stands for `XDOME User Interface Version 1 Iteration #`. The highest iteration

### Branding

This folder contains a simple logo I created for this application

## Development

The subdirectories `xdome-frontend` and `xdome-prototype` are outdated and contain previous iterations of the code.

The subdirectory `xdome-csr-frontend` is the code being actively worked on by Miguel (and myself). [You can view that repository here](https://bitbucket.org/coleg/xdome-csr-frontend/src/master/).

## Exports

In here are subdirectories that are named by the date on which they were created. The most recent date contains the most recent versions of the XDOME UI
