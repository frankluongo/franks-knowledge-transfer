# Budget Microsite

## Assets

This folder contains subdirectories with images, documents, icons, graphics, etc. that were used to help design and build the web page.

## Designs

This folder contains the Budget Microsite Design file made in Sketch, as well as the state budget graphic made in Adobe Illustrator.

## Development

This folder contains the Gatsby.js / React Application for the page. [The code repository can be found here](https://bitbucket.org/frankluongo/budget-visualization-microsite-2020/src/master/).

## Exports

This folder contains all static mock-ups used for reviewing the application with stakeholders
