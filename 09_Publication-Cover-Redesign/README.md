# Publication Cover Redesign

## Assets

This folder contains current report designs to serve as inspiration for new cover designs

## Designs

This folder contains all design work (in Adobe InDesign) for the new LCS Publication Covers

## Exports

This folder contains subdirectories for each round of review for new cover designs
