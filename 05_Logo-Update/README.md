# Logo Update

I found the current LCS logo to be a bit sloppy, so I recreated it in Adobe Illustrator. The new version of the logo is symmetrical and as pixel-perfect as I could make it.
