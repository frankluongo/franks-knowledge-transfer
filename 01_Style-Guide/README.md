# Legislative Council Style Guide

## Design Folder

This folder contains a LCS Design Library created in the Sketch App. I used this to both design the Style Guide Pages as well as create a component library that all other designs drew from.

## Development Folder

This folder contains two subdirectories, `app` and `component-library`. You can ignore the `component-library` one, I was going to use [Storybook.js](https://storybook.js.org/) to build out a legitimate Component Library that would be published to NPM and could used and reused across all projects. Alas, Storybook was overly complicated and not worth completing.

### app Folder

The application within this folder [lives at the lcs-styleguide Repo](https://bitbucket.org/frankluongo/lcs-styleguide/src/master/). It's a React Application built using [parcel.js](https://parceljs.org/). There's a [staging site available through Netlify here](https://lcs-styleguide.netlify.com/).
