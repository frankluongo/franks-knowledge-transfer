# CGAWEB Rewrite

This directory contains all work pertaining to the Colorado General Assembly Website rework project.

## Assets Folder

This folder contains two subdirectories, `Authenticated User Interface` and `Pages`.

### Authenticated User Interface

In here are collections of screenshots from other Legislative Council websites that have some form of users and authentication

### Pages

In here is a collection of all the current pages (as of 12/19ish) on the General Assembly website

## Designs Folder

### Audit

This folder contains the extensive audit I put together of the existing website and all of its components

### User Workflows

This folder contains some potential workflows for users

### Website

This folder contains designs for pages on the reworked website

## Development

In here you'll find the web application I was working on using Next.js and React. [This code is also available in this repo](https://bitbucket.org/frankluongo/coga-website-rewrite-nextjs/src/master/) and you can, as of writing this, [view a preview of the website at this link](https://coga-next-app.now.sh/).

## Exports

This folder simply contains static mock-ups for sharing with stakeholders

## Reference

This folder contains a Photoshop file from Elevated Third that I used to base updated designs on
