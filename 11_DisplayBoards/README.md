# Display Boards

This folder contains a small React application that was created to show a potential future version of the Display Boards interface. [The code for this can be found in this repository](https://bitbucket.org/frankluongo/display-boards-prototype/src/master/).
