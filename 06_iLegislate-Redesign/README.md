# iLegislate Redesign

## Designs

In this folder you'll find designs for a version 2 of iLegislate. The mock-ups include the interface for Electronic Bill Sponsorship and bill notifications

## Development

In this folder, [and at this repository](https://bitbucket.org/frankluongo/ilegislate-v2-prototype/src/master/), you will find the code for a React Prototype of the iLegislate Version 2 interface. The prototype mocks up the interface for bill notifications and electronic bill sponsorship.

At the time of writing this, [the prototype can also be viewed using this link](https://ileg-redesign-prototype.netlify.com/).
